package by.sadovaya.port.runner;

import by.sadovaya.port.creator.ShipCreator;
import by.sadovaya.port.entity.Port;
import by.sadovaya.port.entity.Ship;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.util.ArrayList;


/**
 * Created by Анастасия on 30.09.2015.
 */
public class Runner {
    static {
        new DOMConfigurator().doConfigure("log4j.xml", LogManager.getLoggerRepository());
    }

    private static final Logger LOG = Logger.getLogger(Runner.class);

    public static void main(String[] args) {
        Port port = Port.getInstance();
        ArrayList<Ship> arrShips = new ShipCreator().createArrShips();
        for (Ship i: arrShips){
            i.start();
        }
        LOG.info("\n\n");
    }
}
