package by.sadovaya.port.creator;

import by.sadovaya.port.entity.Ship;

import java.util.ArrayList;

/**
 * Created by Анастасия on 07.10.2015.
 */
public class ShipCreator {
    public ArrayList<Ship> createArrShips (){
        ArrayList<Ship> tempArr = new ArrayList<Ship>();
        for (int i = 0; i < 5; i++){
            tempArr.add(new Ship(i, Ship.AimArrival.UNLOADING, 20, 13));
        }
        for (int i = 0; i < 5; i++){
            tempArr.add(new Ship(i+5, Ship.AimArrival.LOADING, 20, 0));
        }
        for (int i = 0; i < 3; i++){
            tempArr.add(new Ship(i+10, Ship.AimArrival.EXCHANGE, 20, 14+i*2));
        }
        return tempArr;
    }
}
