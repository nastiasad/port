package by.sadovaya.port.collection;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Анастасия on 30.09.2015.
 */
public class BerthQueue <T>{
    private List<T> queue = new LinkedList<T>();
    private int  limit;
    private ReentrantLock lock = new ReentrantLock();
    private Condition isFree = lock.newCondition();

    public BerthQueue(int limit){
        this.limit = limit;
    }

    public void put (T item) throws InterruptedException{
        lock.lock();
        try {
            while (this.queue.size() == this.limit) {
                isFree.await();
            }
            if (this.queue.size() == 0) {
                isFree.signalAll();
            }
            this.queue.add(item);
        } finally {
            lock.unlock();
        }
    }
    public T take () throws InterruptedException {
        lock.lock();
        try {
            while (this.queue.size() == 0){
                isFree.await();
            }
            if (this.queue.size() == this.limit){
                isFree.signalAll();
            }
            T item = this.queue.remove(0);
            return item;
        } finally {
            lock.unlock();
        }
    }
}
