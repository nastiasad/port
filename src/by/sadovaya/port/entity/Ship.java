package by.sadovaya.port.entity;

import org.apache.log4j.Logger;

import java.util.concurrent.Exchanger;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Анастасия on 30.09.2015.
 */
public class Ship extends Thread{
    public enum AimArrival {LOADING, UNLOADING, EXCHANGE};
    private final int ID;

    private AimArrival aimArrival;
    private final int SHIP_CAPACITY;
    private static final int TIME_LIMIT_EXCHANGE = 500;
    private int numContainers;

    private static Exchanger<Integer> exchanger = new Exchanger<Integer>();

    private ReentrantLock lock = new ReentrantLock();

    private static final Logger LOG = Logger.getLogger(Ship.class);

    public Ship (int id, AimArrival aimArrival, int shipCapacity, int numContainers){
        this.ID = id;
        this.aimArrival = aimArrival;
        SHIP_CAPACITY = shipCapacity;
        this.numContainers = numContainers;
    }

    public void run() {
        Berth berth = null;
        try {
            berth = Port.getInstance().takeBerth();
            LOG.info(toString() + "took a berth " + berth.toString());
            switch (aimArrival){
                case LOADING:
                    Port.getInstance().loadToShip(this);
                    break;
                case UNLOADING:
                    Port.getInstance().unloadFromShip(this);
                    break;
                case EXCHANGE:
                    try {
                        LOG.info(toString() + " has amount of containers " + numContainers + " before exchange.");
                        numContainers = exchanger.exchange(numContainers, TIME_LIMIT_EXCHANGE, TimeUnit.MILLISECONDS);
                        LOG.info(toString() + " has amount of containers " + numContainers + " after exchange.");
                    } catch (TimeoutException e) {
                        LOG.info("No other ship for exchange; waiting time ends.");
                    }
                    break;
            }
            if (berth != null) {
                Port.getInstance().makeBerthFree(berth);
                LOG.info(berth.toString() + "is free.");
            }
        } catch (InterruptedException e) {
            LOG.error("Interrupted exception appeared.", e);
        }
    }

    public int getShipCapacity() {
        return SHIP_CAPACITY;
    }

    public int getNumContainers() {
        return numContainers;
    }

    public void setNumContainers(int numContainers) {
        this.numContainers = numContainers;
    }

    public int getID() {
        return ID;
    }

    public String toString (){
        return "Ship " + ID + " ";
    }
}
