package by.sadovaya.port.entity;

import java.util.Random;

/**
 * Created by Анастасия on 30.09.2015.
 */
public class Berth {
    private final int ID;

    public Berth (int id){
        this.ID = id;
    }

    public int getID() {
        return ID;
    }

    public String toString(){
        return "Berth " + ID + " ";
    }
}
