package by.sadovaya.port.entity;

import by.sadovaya.port.collection.BerthQueue;
import org.apache.log4j.Logger;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Анастасия on 30.09.2015.
 */
public class Port {
    private static Port instance;
    private static AtomicBoolean instanceCreated = new AtomicBoolean(false);
    private static ReentrantLock lock = new ReentrantLock();
    private Condition condLoad = lock.newCondition();
    private Condition condUnLoad = lock.newCondition();
    private static final Logger LOG = Logger.getLogger(Ship.class);

    public final int NUM_BERTH = 3;
    public final int STORAGE_CAPACITY = 50;
    private final BerthQueue<Berth> freeBerths = new BerthQueue<Berth>(3);
    private int numContainers;

    private Port (){
        for(int i = 0; i < NUM_BERTH; i++){
            try {
                freeBerths.put(new Berth(i + 1));
            } catch (InterruptedException e) {
                LOG.error("Interrupted exception appeared at the moment of port creation.", e);
            }
        }
        numContainers = 0;
    }

    public static Port getInstance (){
        if(!instanceCreated.get()) {
            lock.lock();
            try {
                if (!instanceCreated.get()) {
                    instance = new Port();
                    instanceCreated.getAndSet(true);
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    public Berth takeBerth () throws InterruptedException {
        return freeBerths.take();
    }

    public void makeBerthFree (Berth berth) throws InterruptedException{
            freeBerths.put(berth);
    }

    public void unloadFromShip(Ship ship){
        try {
            lock.lock();
            int numCont = ship.getNumContainers();
            int leftInStorage = STORAGE_CAPACITY - numContainers;
            if (numCont > leftInStorage){
                condUnLoad.await(500, TimeUnit.MILLISECONDS);
            }

            if (numCont <= leftInStorage) {
                numContainers = numContainers + numCont;
                ship.setNumContainers(0);
                LOG.info(ship.toString() + "fully unloaded!");
                condLoad.signalAll();
            } else {
                LOG.info(ship.toString() + "NOT fully unloaded!");
            }
        } catch (InterruptedException e) {
            LOG.error("Interrupted exception appeared.", e);
        } finally {
            lock.unlock();
        }
    }

    public void loadToShip (Ship ship) {
        try{
            lock.lock();
            int shipCap = ship.getShipCapacity();
            if(shipCap > numContainers){
                condLoad.await(500, TimeUnit.MILLISECONDS);
            }
            if(shipCap <= numContainers) {
                ship.setNumContainers(shipCap);
                numContainers = numContainers - shipCap;
                LOG.info(ship.toString() + "fully loaded!");
                condUnLoad.signalAll();
            } else {
                LOG.info(ship.toString() + "NOT fully loaded!");
            }
        } catch (InterruptedException e) {
            LOG.error("Interrupted exception appeared.", e);
        } finally {
            lock.unlock();
        }
    }
}
